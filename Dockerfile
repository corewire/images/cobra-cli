FROM golang:1.19-alpine AS final
WORKDIR /go/src
RUN apk add --no-cache git bash 
RUN go install github.com/spf13/cobra-cli@latest
ENTRYPOINT [ "cobra-cli" ]
