# cobra-cli


Containerized cobra-cli:  https://github.com/spf13/cobra-cli 

Usage: 
```
docker build -t cobra-cli .   
```
```
docker run --rm -v $(pwd):/go/src registry.gitlab.com/corewire/images/cobra-cli
```
